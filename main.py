import os
# in terminal run command: pip install pillow
from PIL import Image

directory = 'D:\\diplom\\resize_image\\plosko-valgusnaya\\'

width = 299  # min width in pixels to resize images in folder
height = 299  # min height in pixels to resize images in folder


with os.scandir(directory) as it:
    for entry in it:
        if not entry.is_file():
            print("dir:\t" + entry.name)
        else:
            print("file:\t" + entry.name)
            img_obj = Image.open(directory + entry.name)
            img = img_obj.resize((width, height), Image.ANTIALIAS)
            img.save(directory + entry.name)
